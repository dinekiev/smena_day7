"""day7 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from api import api_today, api_date_by_week_day, api_week_day_by_date, api_date_range, api_salary_percents

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^date/today', api_today),
    url(r'^date-by-week-day/$', api_date_by_week_day),
    url(r'^week-day-by-date/$', api_week_day_by_date),
    url(r'^date-range/$', api_date_range),
    url(r'^salary-percents/$', api_salary_percents)
]
