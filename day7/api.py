# encoding: utf-8

import datetime
# import locale

from django.http import HttpResponse

def api_today(request):
    # locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
    return HttpResponse(datetime.datetime.now().strftime('%d %B %Y'))


def api_date_by_week_day(request):
    week_days = {'monday': 0, 'tuesday': 1, 'wednesday': 2, 'thursday': 3, 'friday': 4, 'saturday': 5, 'sunday': 6}

    weekday = request.GET.get('weekday', '')
    if weekday == '':
        return HttpResponse("parameter 'weekday' is missing")

    if week_days.has_key(weekday):
        offset = week_days[weekday] - datetime.date.today().weekday()
        return HttpResponse( (datetime.date.today() + datetime.timedelta(offset)).strftime('%d.%m.%Y'))
    else:
        return HttpResponse('invalid argument: {0}'.format(weekday))


def api_week_day_by_date(request):
    date = request.GET.get('date', '')
    if date == '':
        return HttpResponse("parameter 'date' is missing")

    try:
       d = datetime.datetime.strptime(date, '%d.%m.%Y').weekday()
    except ValueError:
       return HttpResponse("Incorrect data format, should be DD.MM.YYYY")

    week_days = {0: u'Понедельник', 1: u'Вторник', 2: u'Среда', 3: u'Четверг', 4: u'Пятница', 5: u'Суббота', 6: u'Воскресенье'}
    return HttpResponse(week_days[d])


def api_date_range(request):
    str_date_begin = request.GET.get('date-begin', '')
    str_date_end = request.GET.get('date-end', '')

    if str_date_begin == '':
        return HttpResponse("parameter 'date-begin' is missing")

    if str_date_end == '':
        return HttpResponse("parameter 'date-end' is missing")

    try:
       date_begin = datetime.datetime.strptime(str_date_begin, '%d.%m.%Y')
       date_end = datetime.datetime.strptime(str_date_end, '%d.%m.%Y')
    except ValueError:
       return HttpResponse("Incorrect data format, should be DD.MM.YYYY")

    return HttpResponse(abs((date_begin - date_end).days))


def api_salary_percents(request):
    str_salary = request.GET.get('salary', '')
    try:
        salary = float(str_salary)
    except ValueError:
        return HttpResponse("Salary value is incorrect")

    str_acts = request.GET.get('acts', '');
    acts = str_acts.split(',')

    val = float(0)
    s = ''
    total = float(0)
    if 'pens' in acts:
        val = (salary / 100) * 22
        total = total + val
        s = s + 'Пенсионный фонд: {0}\n'.format(val)
    if 'socstrah' in acts:
        val = (salary / 100) * 2.9
        total = total + val
        s = s + 'Фонд социального страхования: {0}\n'.format(val)
    if 'medstrah' in acts:
        val = (salary / 100) * 5.1
        total = total + val
        s = s + 'Федеральный фонд обязательного медицинского страхования: {0}\n'.format(val)
    if 'strah' in acts:
        val = (salary / 100) * 0.2
        total = total + val
        s = s + 'Страховой   налог   от   несчастного   случая   на   производстве для служащих: {0}\n'.format(val)
    if val > 0:
        s = s + 'Итог: {0}'.format(total)

    return HttpResponse(s)
